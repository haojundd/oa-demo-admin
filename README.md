# oa-demo-admin

## 简介

本项目为 OA Demo 系统的后台管理端部分，基于 Vue3 + Vite5 + pinia + ts + less + ant-design-vue4 开发

## 配套项目

后端项目 https://gitee.com/haojundd/oa-demo-server.git

前端项目 https://gitee.com/haojundd/oa-demo-admin.git

微信小程序 https://gitee.com/haojundd/oa-demo-wxapp.git

## Vite配置

[Vite Configuration Reference](https://vitejs.dev/config/).

## 安装

推荐[安装 pnpm](https://pnpm.io/zh/installation)

```sh
pnpm install
```

### 启动

```sh
pnpm dev
```

### 打包

```sh
pnpm build
```

## 项目结构

```text
├── public                   # 静态资源
├── src
│   ├── api                  # 接口
│   ├── assets               # 图片资源
│   ├── components           # 全局组件
│   ├── enums                # 常量
│   ├── hooks                # 钩子
│   ├── layout               # 布局
│   ├── router               # 路由
│   ├── store                # 存储库
│   ├── styles               # 全局样式
│   ├── types                # 类型声明
│   ├── utils                # 工具
│   ├── views                # 所有页面
│   ├── App.vue              # 入口页面
│   └── main.ts              # 初始化
├── .env                     # 环境变量
├── .env.production          # 生产环境变量
├── .env.uat                 # UAT环境变量
├── env.d.ts                 # 环境变量声明
├── index.html               # 入口文件
├── tsconfig.json            # typescript 配置
└── vite.config.ts           # vite 配置
```

## 界面预览

![](doc-imgs/login.png)

![](doc-imgs/user-edit.png)

![](doc-imgs/role.png)

![](doc-imgs/role-edit.png)

![](doc-imgs/dept-edit.png)

![](doc-imgs/post.png)

![](doc-imgs/absense.png)

![](doc-imgs/attend.png)

![](doc-imgs/attend-audit.png)

![](doc-imgs/expense.png)

![](doc-imgs/notice-edit.png)

![](doc-imgs/dict.png)

![](doc-imgs/log.png)

![](doc-imgs/log-d.png)

