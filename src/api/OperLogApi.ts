import request from './http';

/**
 * 操作日志 前端接口
 *
 * @author haojun
 * @since 2023-08-10
 */
export default class OperLogApi {
  static page(data: OperLog) {
    let { index, size, ...vo } = data;
    return request.post<OperLog>(`/operLog/page/${index}/${size}`, vo);
  }

  static delete(ids: number[]) {
    return request.post('/operLog/delete', ids);
  }

  static truncate() {
    return request.post('/operLog/truncate');
  }
}
