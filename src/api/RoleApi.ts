import request from './http';

/**
 * 角色信息 前端接口
 *
 * @author haojun
 * @since 2023-07-25
 */
export default class RoleApi {
  static page(data: Role) {
    let { index, size, ...vo } = data;
    return request.post(`/role/page/${index}/${size}`, vo);
  }

  static list(data: Role) {
    return request.post<Role>('/role/list', data);
  }

  static save(data: Role) {
    return request.post<Role>('/role', data);
  }

  static update(data: Role) {
    return request.put<Role>('/role', data);
  }

  static delete(ids: number[]) {
    return request.post('/role/delete', ids);
  }

  static checked(roleId: number) {
    return request.get(`/role/checked/${roleId}`);
  }
}
