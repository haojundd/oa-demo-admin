import request from './http';

/**
 * 菜单信息 前端接口
 *
 * @author haojun
 * @since 2023-07-25
 */
export default class MenuApi {
  static listAndTree() {
    return request.get('/menu/list-tree');
  }

  static save(data: Menu) {
    return request.post<Menu>('/menu', data);
  }

  static update(data: Menu) {
    return request.put<Menu>('/menu', data);
  }

  static delete(ids: number[]) {
    return request.post('/menu/delete', ids);
  }
}
