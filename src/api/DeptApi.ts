import request from './http';

/**
 * 部门表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class DeptApi {
  static page(data: Dept) {
    let { index, size, ...vo } = data;
    return request.post(`/dept/page/${index}/${size}`, vo);
  }

  static list(data: Dept) {
    return request.post('/dept/list', data);
  }

  static getById(id: number) {
    return request.get(`/dept/${id}`);
  }

  static save(data: Dept) {
    return request.post('/dept', data);
  }

  static update(data: Dept) {
    return request.put('/dept', data);
  }

  static delete(ids: number[]) {
    return request.post('/dept/delete', ids);
  }

  static tree() {
    return request.get('/dept/tree');
  }
}
