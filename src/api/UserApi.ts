import request from './http';

/**
 * 用户信息 前端接口
 *
 * @author haojun
 * @since 2022-06-12
 */
export default class UserApi {
  static page(data: User) {
    let { index, size, ...vo } = data;
    return request.post<User>(`/user/page/${index}/${size}`, vo);
  }

  static save(data: User) {
    return request.post<User>('/user', data);
  }

  static update(data: User) {
    return request.put<User>('/user', data);
  }

  static delete(ids: number[]) {
    return request.post('/user/delete', ids);
  }

  static changeAvatar(data: object) {
    return request.post('/user/changeAvatar', data);
  }

  static changePwd(data: object) {
    return request.post('/user/changePwd', data);
  }

  static resetPwd(data: object) {
    return request.post('/user/resetPwd', data);
  }

  static getRoles(userId: number) {
    return request.get(`/user/roles/${userId}`);
  }

  static export() {
    return request.postBlob('/user/export');
  }

  static userOpts(keyword: string) {
    return request.get('/user/select-user', { params: { keyword } });
  }
}

export function captcha() {
  return request.getBlob('/captcha');
}

export function login(data: object) {
  return request.post('/login', data);
}

export function getInfo() {
  return request.get<User>('/current');
}

export function logout() {
  return request.post('/logout');
}
