export const TOKEN_KEY = 'Authorization';
export const BASE_HOME = '/';
export const LOGIN_PATH = '/login';

export enum MenuTypeEnum {
  DIR = 'DIR',
  PAGE = 'PAGE',
  BTN = 'BTN',
}
