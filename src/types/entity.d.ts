declare interface LoginForm {
  userId: number; // 工号
  password: string; // 密码
  captcha: string; // 验证码
}

declare interface User {
  userId?: number; // 工号
  nickname?: string; // 姓名
  idcard?: string; // 身份证
  phone?: string; // 手机
  email?: string; // 邮箱
  avatar?: string; // 头像
  leader?: string; // 直属上级
  leaderId?: number; // 直属上级ID
  deptId?: number; // 部门ID
  dept?: string; // 部门
  postId?: number; // 职位ID
  post?: string; // 职位
  version?: number; // 乐观锁
  deleted?: boolean; // 逻辑删除
  leaveTime?: string; // 离职时间
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间
  permissions?: string[];
  roleIds?: number[];

  [key: string]: any;
}

declare interface Role {
  roleId?: number; // 主键
  roleName?: string; // 角色名称
  roleSort?: number; // 显示顺序
  version?: number; // 乐观锁
  deleted?: boolean; // 逻辑删除
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间
  menuIds?: number[];

  [key: string]: any;
}

declare interface Menu {
  menuId?: number; // 主键
  menuName?: string; // 菜单名称
  permission?: string; // 菜单权限
  menuType?: 'DIR' | 'PAGE' | 'BTN'; // 菜单类型
  parentId?: number; // 父菜单ID
  version?: number; // 乐观锁
  deleted?: boolean; // 逻辑删除
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

declare interface OperLog {
  operId?: number; // 日志主键
  requestMethod?: string; // 请求方式
  operUrl?: string; // 请求URL
  className?: string; // 类名
  method?: string; // 方法名称
  operParam?: string; // 请求参数
  jsonResult?: string; // 返回参数
  status?: number; // 操作状态（1正常 0异常）
  operName?: string; // 操作人员
  errorMsg?: string; // 错误消息
  operTime?: string; // 操作时间

  [key: string]: any;
}

/**
 * 部门表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Dept {
  deptId?: number; // 主键
  deptName?: string; // 部门名称
  parentId?: number; // 父菜单ID
  deptSort?: number; // 显示顺序
  version?: number; // 乐观锁
  deleted?: any; // 逻辑删除
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

/**
 * 岗位表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Post {
  postId?: number; // 主键
  postName?: string; // 岗位名称
  postSort?: number; // 显示顺序
  version?: number; // 乐观锁
  deleted?: any; // 逻辑删除
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

/**
 * 通知表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Notice {
  id?: number; // 主键
  title?: string; // 标题
  content?: string; // 内容
  version?: number; // 乐观锁
  deleted?: any; // 逻辑删除
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

/**
 * 报销表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Expense {
  id?: number; // 主键
  userId?: number; // 用户ID
  amount?: any; // 报销金额
  status?: any; // 申请状态
  remark?: string; // 费用说明
  annex?: string; // 附件
  expenseTime?: string; // 消费日期
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

/**
 * 考勤表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Attendance {
  id?: number; // 主键
  userId?: number; // 用户ID
  status?: string; // 状态
  type?: string; // 异常类型
  remark?: string; // 备注
  snapshot?: string; // 拍照
  lng?: string; // 经度
  lat?: string; // 纬度
  signDate?: string; // 签到日期
  signTime?: string; // 签到时间
  exitTime?: string; // 签退时间

  [key: string]: any;
}


/**
 * 请假表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2023-12-13
 */
declare interface Absence {
  absenceId?: number; // 主键
  userId?: number; // 用户ID
  type?: any; // 请假类型
  remark?: string; // 说明
  status?: any; // 状态
  version?: number; // 乐观锁
  deleted?: any; // 逻辑删除
  startTime?: string; // 开始时间
  endTime?: string; // 结束时间
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}

/**
 * 字典表 类型声明 粘到 src/types/entity.d.ts
 *
 * 2024-01-03
 */
declare interface Dict {
  id?: number; // 主键
  name?: string; // 名称
  value?: string; // 值
  createTime?: string; // 创建时间
  updateTime?: string; // 更新时间

  [key: string]: any;
}
