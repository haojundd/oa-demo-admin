declare interface MyResult<T> {
  code: string;
  message: string;
  exception?: string;
  total?: number;
  data: T[] | any;
}
