import type { RouteRecordRaw } from 'vue-router';
import { createRouter, createWebHistory } from 'vue-router';
import type { App } from 'vue';
import { h } from 'vue';
import { createRouterGuards } from '@/router/guard';
import Layout from '../layout/index.vue';
import {
  AuditOutlined,
  ControlOutlined,
  DashboardOutlined,
  ProfileOutlined,
  ProjectOutlined,
  NotificationOutlined,
} from '@ant-design/icons-vue';

/**
 * meta: {
 *   title: 页面标题
 *   permissions: 页面权限
 *   icon: 图标
 *   hidden: 是否在菜单栏隐藏
 *   alwaysShow: 只有一个子菜单是否显示自身
 *   keepAlive: 是否缓存页面
 * }
 */
export const ErrorRoute: RouteRecordRaw = {
  path: '/:path(.*)*',
  name: 'ErrorPage',
  component: () => import('@/views/404.vue'),
  meta: {
    title: 'ErrorPage',
  },
};

export const ConstantRoute: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
    meta: { title: '登陆' },
  },
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'home',
    component: Layout,
    redirect: '/home/index',
    meta: {
      title: 'Dashboard',
      icon: () => h(DashboardOutlined),
    },
    children: [
      {
        path: 'index',
        name: 'Home',
        component: () => import('@/views/Home.vue'),
        meta: { title: '首页' },
      },
    ],
  },
  {
    path: '/redirect',
    name: 'redirect',
    component: Layout,
    meta: {
      title: 'Redirect',
    },
    children: [
      {
        path: '/redirect/:path(.*)',
        name: 'Redirect',
        component: () => import('@/views/redirect/index.vue'),
        meta: {
          title: 'Redirect',
        },
      },
    ],
  },
];

export const AuthRoute: RouteRecordRaw[] = [
  {
    path: '/notice',
    name: 'notice',
    component: Layout,
    meta: {
      icon: () => h(NotificationOutlined),
      permissions: ['notice:query'],
    },
    children: [
      {
        path: 'index',
        name: `NoticePage`,
        meta: {
          title: '通知管理',
        },
        component: () => import('@/views/sys/NoticePage.vue'),
      },
    ],
  },
  {
    path: '/system',
    name: 'system',
    component: Layout,
    meta: {
      title: '系统管理',
      icon: () => h(ControlOutlined),
      permissions: ['user:query', 'role:query', 'menu:query'],
    },
    redirect: '/system/user',
    children: [
      {
        path: '/system/user',
        name: 'UserPage',
        component: () => import('@/views/sys/UserPage.vue'),
        meta: { title: '用户管理', permissions: ['user:query'] },
      },
      {
        path: '/system/role',
        name: 'RolePage',
        component: () => import('@/views/sys/RolePage.vue'),
        meta: { title: '角色管理', permissions: ['role:query'] },
      },
      {
        path: '/system/menu',
        name: 'MenuPage',
        component: () => import('@/views/sys/MenuPage.vue'),
        meta: { title: '权限管理', permissions: ['menu:query'] },
      },
      {
        path: '/system/dept',
        name: 'DeptPage',
        component: () => import('@/views/sys/DeptPage.vue'),
        meta: { title: '部门管理', permissions: ['dept:query'] },
      },
      {
        path: '/system/post',
        name: 'PostPage',
        component: () => import('@/views/sys/PostPage.vue'),
        meta: { title: '岗位管理', permissions: ['post:query'] },
      },
      {
        path: '/system/dict',
        name: 'DictPage',
        component: () => import('@/views/sys/DictPage.vue'),
        meta: { title: '字典管理', permissions: ['dict:query'] },
      },
    ],
  },
  {
    path: '/approval',
    name: 'approval',
    component: Layout,
    meta: {
      title: '审批管理',
      icon: () => h(AuditOutlined),
      permissions: ['attend:query', 'absence:query', 'expense:query'],
    },
    children: [
      {
        path: '/system/attendance',
        name: 'AttendancePage',
        component: () => import('@/views/sys/AttendancePage.vue'),
        meta: { title: '考勤管理', permissions: ['attend:query'] },
      },
      {
        path: '/system/absence',
        name: 'AbsencePage',
        component: () => import('@/views/sys/AbsencePage.vue'),
        meta: { title: '请假管理', permissions: ['absence:query'] },
      },
      {
        path: '/system/expense',
        name: 'ExpensePage',
        component: () => import('@/views/sys/ExpensePage.vue'),
        meta: { title: '报销管理', permissions: ['expense:query'] },
      },
    ],
  },
  {
    path: '/log',
    name: 'log',
    component: Layout,
    meta: {
      icon: () => h(ProfileOutlined),
      permissions: ['*'],
    },
    children: [
      {
        path: 'operation',
        name: `OperLogPage`,
        meta: {
          title: '操作日志',
        },
        component: () => import('@/views/log/OperLogPage.vue'),
      },
    ],
  },
  {
    path: '/about',
    name: 'about',
    component: Layout,
    meta: {
      icon: () => h(ProjectOutlined),
      permissions: ['*'],
    },
    children: [
      {
        path: 'index',
        name: `about_index`,
        meta: {
          title: '关于',
        },
        component: () => import('@/views/about/index.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: ConstantRoute,
});

export function setupRouter(app: App) {
  app.use(router);
  // 创建路由守卫
  createRouterGuards(router);
}

export default router;
