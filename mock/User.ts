import { MockMethod } from 'vite-plugin-mock';
import Mock from 'mockjs';

const list = [];
const count = 100;
/**
 * 拓展mock
 */
Mock.Random.extend({
  phone: function () {
    const phonePrefixs = ['139', '155', '172']; // 手机号前缀
    return this.pick(phonePrefixs) + Mock.mock(/\d{8}/); //Number()
  },
});

for (let i = 0; i < count; i++) {
  list.push(
    Mock.mock({
      userId: '@increment',
      nickname: '@cname',
      phone: '@phone',
      email: '@email',
      locked: '@boolean',
      permissions: ['*'],
      roles: ['*'],
      createTime: '@datetime',
      updateTime: '@datetime',
    })
  );
}

export default [
  {
    url: /\/api\/user\/page\/\d+\/\d+/,
    method: 'post',
    timeout: 300,
    response: () => {
      const pageIndex = 1;
      const pageSize = 10;
      const pageList = list.filter(
        (item, index) => index < pageSize * pageIndex && index >= pageSize * (pageIndex - 1)
      );

      return {
        code: 'SUCCESS',
        message: '成功',
        data: pageList,
        total: count,
      };
    },
  },
  {
    url: '/api/user',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/user/delete',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/user',
    method: 'put',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: /\/api\/user\/roles\/\d+/,
    method: 'get',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
        data: [1],
        total: 1,
      };
    },
  },
] as MockMethod[];
